#!/usr/bin/env python
"""
Main Control Node
"""
import rospy
import numpy as np
import os
from mas507.msg import ServoSetpoints
from mas507.msg import vectors
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector

if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

        # Publishers
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)

        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Callbacks
        def vectors_callback(msg):
            global aruco_z
            global aruco_x
            global aruco_y
            global aruco_id
            aruco_z = msg.z
            aruco_y = msg.y
            aruco_id = msg.id
            aruco_x = msg.x
        
        # Straight Drive
        def straight_drive(meter):
            t_end = 100*meter
            t=0
            """servoSetpoints.leftWheel = 346
            servoSetpoints.rightWheel = 315.9
            pub_servoSetpoints.publish(servoSetpoints)
            while t < t_end:
                rate.sleep()
                t += 1
            servoSetpoints.leftWheel = 327
            servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)"""
            while t < t_end:
                #print aruco_x
                if aruco_x > 0.01:
                    servoSetpoints.leftWheel = 361
                    servoSetpoints.rightWheel = 310
                    #print "svinger"
                elif aruco_x < -0.01:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 300
                    #print "svinger feil vei"
                else:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 310
                    #print "rett fram"
                pub_servoSetpoints.publish(servoSetpoints)
                rate.sleep()
                t += 1
            servoSetpoints.leftWheel = 327
            servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)

        # Straight Reverse
        def straight_reverse(meter):
            t_end = 100*meter*1.153946
            t=0
            servoSetpoints.leftWheel = 314
            servoSetpoints.rightWheel = 346
            pub_servoSetpoints.publish(servoSetpoints)
            while t < t_end:
                rate.sleep()
                t += 1
            servoSetpoints.leftWheel = 327
            servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)
            
        # Turn degrees CW
        def turn_cw_degrees(degrees):
            t_end = 0.12222222 * degrees
            t=0
            servoSetpoints.leftWheel = 346
            servoSetpoints.rightWheel = 346
            pub_servoSetpoints.publish(servoSetpoints)
            while t < t_end:
                rate.sleep()
                t += 1
            servoSetpoints.leftWheel = 327
            servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)

            # Turn degrees CCW
        def turn_ccw_degrees(degrees):
            t_end = 0.12222222 * degrees
            t=0
            servoSetpoints.leftWheel = 314.9
            servoSetpoints.rightWheel = 315
            pub_servoSetpoints.publish(servoSetpoints)
            while t < t_end:
                rate.sleep()
                t += 1
            servoSetpoints.leftWheel = 327
            servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)

            # Pick strawberry
        def pick_strawberry():
            in_position = False
            picked = False
            while not picked:
                #picking_length = (227.775 - strawberryDetector.y**2)**0.5
                picking_length = 0.22 + 0.4 * strawberryDetector.y
                #print picking_length
                #print strawberryDetector.z
                #print strawberryDetector.y
                if in_position:
                    for x in range(30):
                        servoSetpoints.servo1 = 575 - 5 * x
                        pub_servoSetpoints.publish(servoSetpoints)
                        rate.sleep()
                    straight_reverse(0.02)
                    servoSetpoints.servo1 = 230
                    pub_servoSetpoints.publish(servoSetpoints)
                    for x in range(3):
                        rate.sleep()
                    servoSetpoints.servo1 = 580
                    pub_servoSetpoints.publish(servoSetpoints)
                    in_position = False
                    picked = True

                elif strawberryDetector.z > picking_length:
                    if strawberryDetector.x > 0.025:
                        servoSetpoints.leftWheel = 348
                        servoSetpoints.rightWheel = 315
                    elif strawberryDetector.x < -0.007:
                        servoSetpoints.leftWheel = 346
                        servoSetpoints.rightWheel = 308
                    else:
                        servoSetpoints.leftWheel = 346
                        servoSetpoints.rightWheel = 315.9
                elif strawberryDetector.z != 0:
                    if strawberryDetector.x > 0.025:
                        servoSetpoints.leftWheel = 341
                        servoSetpoints.rightWheel = 327
                    elif strawberryDetector.x < -0.007:
                        servoSetpoints.leftWheel = 327
                        servoSetpoints.rightWheel = 320
                    else:
                        servoSetpoints.leftWheel = 327
                        servoSetpoints.rightWheel = 327
                        in_position = True
                else:
                    picked = True
                pub_servoSetpoints.publish(servoSetpoints)
                rate.sleep()

            #Check strawberry
        def check_strawberry ():
            check_range = 0.08
            counter = 0
            for x in range(2):
                rate.sleep()
            for x in range(8):
                if strawberryDetector.x > -check_range and strawberryDetector.z < 0.45 and strawberryDetector.z != 0:
                    counter = counter + 1
                rate.sleep()
            if strawberryDetector.x < check_range and strawberryDetector.x > -check_range and counter >= 5:

                #if strawberryDetector.z < 0.35:
                #straight_reverse(0.25-strawberryDetector.z)
                #print strawberryDetector.x
                return True
            else:
                return False

        def turn_to_marker(id):
            while abs(aruco_x) > 0.2 or aruco_id != id:
                turn_ccw_degrees(10)
                for x in range(3):
                    rate.sleep()
        
        #Subscribers
        rospy.Subscriber("vectors", vectors, vectors_callback)
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)

        # Start Synchronous ROS node execution
        rate = rospy.Rate(10)
        in_position = False
        #number_of_arucos_seen = 0
        aruco_z = 0
        aruco_x = 0
        aruco_y = 0
        aruco_id = 0
        id_treshold = 0.26
        strawber = False
        servoSetpoints = ServoSetpoints()
        rate.sleep()
        servoSetpoints.servo1 = 580
        pub_servoSetpoints.publish(servoSetpoints)
        rate.sleep()
        straight_drive(0.075)
        state = 1

        while not rospy.is_shutdown():
            #print aruco_id
            if aruco_id == 1 and aruco_z > id_treshold and state == 1:
                turn_cw_degrees(90)
                strawber = check_strawberry()
                if strawber == True:

                    if strawberryDetector.z < 0.35:
                        straight_reverse(0.30-strawberryDetector.z)
                    pick_strawberry()
                    straight_reverse(0.05)
                    turn_to_marker(1)
                    strawber = False
                else:
                    turn_to_marker(1)
                    straight_drive(0.05)
                    #print "kjoerer mot id 1"
                for x in range(2):
                    rate.sleep()
                
            elif aruco_id == 1 and state == 1:
                turn_cw_degrees(70)
                state = 2
            
            elif aruco_id == 2 and aruco_z > id_treshold and state == 2:
                straight_drive(0.05)
            elif aruco_id == 2 and state == 2:
                turn_cw_degrees(60)
                state = 3

            elif aruco_id == 3 and aruco_z > id_treshold and state == 3:
                turn_cw_degrees(90)
                if check_strawberry():
                    if strawberryDetector.z < 0.35:
                        straight_reverse(0.30-strawberryDetector.z)
                    pick_strawberry()
                    straight_reverse(0.05)
                    turn_to_marker(3)
                else:
                    turn_to_marker(3)
                    straight_drive(0.05)
                for x in range(10):
                    rate.sleep()
            elif aruco_id == 3 and state == 3:
                print "ferdig"
                state = 4
            elif state != 4:
                turn_cw_degrees(20)
                for x in range(2):
                    rate.sleep()
            


            

            """  if in_position:
                for x in range(40):
                    servoSetpoints = ServoSetpoints()
                    servoSetpoints.servo1 = 575 - 5 * x
                    pub_servoSetpoints.publish(servoSetpoints)
                    rate.sleep()
                servoSetpoints.servo1 = 580
                servoSetpoints.leftWheel = 315
                servoSetpoints.rightWheel = 346
                pub_servoSetpoints.publish(servoSetpoints)
                for x in range(2):
                    rate.sleep()
                servoSetpoints.leftWheel = 310
                servoSetpoints.rightWheel = 351
                pub_servoSetpoints.publish(servoSetpoints)
                while aruco_id == None:
                    rate.sleep()
                servoSetpoints.leftWheel = 327
                servoSetpoints.rightWheel = 327
                pub_servoSetpoints.publish(servoSetpoints)
                in_position = False

            if strawberryDetector.z > 0.3:
                if strawberryDetector.x > 0.025:
                    servoSetpoints.leftWheel = 356
                    servoSetpoints.rightWheel = 310
                elif strawberryDetector.x < -0.007:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 305
                else:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 310
            elif strawberryDetector.z > 0.21:
                if strawberryDetector.x > 0.025:
                    servoSetpoints.leftWheel = 348
                    servoSetpoints.rightWheel = 315
                elif strawberryDetector.x < -0.007:
                    servoSetpoints.leftWheel = 346
                    servoSetpoints.rightWheel = 308
                else:
                    servoSetpoints.leftWheel = 346
                    servoSetpoints.rightWheel = 315
            elif strawberryDetector.z != 0:
                if strawberryDetector.x > 0.025:
                    servoSetpoints.leftWheel = 341
                    servoSetpoints.rightWheel = 327
                elif strawberryDetector.x < -0.007:
                    servoSetpoints.leftWheel = 327
                    servoSetpoints.rightWheel = 320
                else:
                    servoSetpoints.leftWheel = 327
                    servoSetpoints.rightWheel = 327
                    in_position = True
            elif aruco_z > 0.3:
                if aruco_x > 0.1:
                    servoSetpoints.leftWheel = 361
                    servoSetpoints.rightWheel = 310
                elif aruco_x < -0.1:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 300
                else:
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 310
            elif aruco_z != 0:
                if number_of_arucos_seen < 3:
                    id_old = aruco_id
                    servoSetpoints.leftWheel = 361
                    servoSetpoints.rightWheel = 310
                    pub_servoSetpoints.publish(servoSetpoints)
                    while aruco_id == id_old or None:
                        rate.sleep()
                    servoSetpoints.leftWheel = 351
                    servoSetpoints.rightWheel = 310
                    number_of_arucos_seen += 1
                else:
                    print "finished"
            else:
                servoSetpoints.leftWheel = 327
                servoSetpoints.rightWheel = 327
            pub_servoSetpoints.publish(servoSetpoints)"""
            #print strawberryDetector.x
            #print strawberryDetector.y
            # Sleep remaining time
            rate.sleep()

    except rospy.ROSInterruptException:
        pass