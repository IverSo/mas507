Project Description
====================
In this project an autonomous strawberry picking robot is to be developed. 
This process includes design of the robot arm and coding of the robot software.
An assembled robot base was provided by the lecturer, complete with two servo motors with motor control board, an Nvidia Jestson Nano with a camera setup
and a pre-designed frame for the robot. Extra servo motors were on hand for actuating the arm design.

To complete the project, the following set of functions needed to be developed.

#. Design an arm for picking strawberries.
#. Develop code to be able to control the arm so that the berries get picked.
#. Develop a system for detecting strawberries and move to the correct position for picking.
#. Develop a system for detecting and following Aruco markers

Strawberry picking setup
--------------------------
The robot will be tested on an approximation of a strawberry field, where five plastic bushes is put in a row. The bushes will have plastic
strawberries connected to the bush with a weak magnet. The strawberries comes in two colours, red and green.
The robot is intended to only pick the red strawberries. A path around the the strawberry bushes is marked by three Aruco markers, and is to be followed. 
The test setup is described in :numref:`StrawberryPath`.

.. figure:: figures/StrawberryPath.jpg
   :name: StrawberryPath
   :scale: 30 %
   :align: center
   
   Representation of test setup for the strawberry picking robot.
