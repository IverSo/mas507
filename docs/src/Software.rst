Jetbot Software
================
Developing for a robot to do certain tasks often seems easy when a human visualizes the sequences. 
Terms such as “just pick up” and “turn back the way it came from” can lead to underestimation of how complex a robot code can become. 
To develop a code, an overall plan must be carefully layed to make it easier for the programers down the road. 
A poor overall plan for the program can easily lead to a heaps of complicated parts, witch  in turn can lead to enormous frustration. 
Partitioning of the code into sequences and states, making them unique for all the states with their specific measurements and signature signs, 
is eventually the main goal when writing the code as it can never be too “idiotproof”.

State Chart
-------------
The tasks the robot needed to do were split into 3 parts, which was called State1, State2 and State3. 
A state variable were used to differentiate between the different jobs the robot would need to do. 
By making a counter tick up when the robot reached a certain gold, robot were able to moved between states. 
Seen from the picture :numref:`statezone`, the state system were designed to make the robot operate with its specific state tasks until it a certain distance from the Aruco marker. 
When the robot switched to the next state, it would look for a new Aruco marker and based on the state, engage in a certain way.

 .. figure:: figures/statezone.jpg
   :name: statezone
   :scale: 70 %
   :align: center
   
   The zones indicating the states the robot travels thought.

Main behavior
^^^^^^^^^^^^^^^
The robot always had a state variable, which made the robot respond only to the state’s assigned Aruco marker. 
However, when the robot have no Aruco marker in sight and are not completing a picking or checking sequence, a sequence where the robot is turn 10° clockwise and checks for a Aruco marker is activated. 
The robot then takes a short pause during the turning to look for the marker and then repeating the turn sequence until the Aruco marker is found again. 
The short pause were done for the camera to more easily detect the Aruco markers.

State 1
^^^^^^^^
The first states indicate the start of the program. 
The starting line is where the robot starts it course.
The robot is then supposed to pick up the strawberries on the left side of the plants until its 25cm away from marker 1. 
During the state, the marker, which is located at the end of the strawberry bushes, is used as a compass for the robot stays on the right course.
The robot uses an Aruco marker detection algorithm to locate, read id number and display the markers coordinate referenced from the robot’s camera. 

When the robot is in state 1, it turns 90° away from the course towards the strawberry bushes, 
stops and checks if it sees a strawberry, by using the strawberry detection algorithm. 
The coordinates of the berry has to be within a certain range, which makes the robot avoid trying to get berries further down the course.  
If a strawberry is detected, the robot checks its distance from the strawberry. 
If it is too close to the strawberry the robot reverses straight until it is a certain distance from the strawberry. 
A sequence for picking up the strawberry is then initiated by using the coordinates from the strawberry detection algorithm and then making the robot position itself close to the strawberry. 
The robot then uses its arm to pick up the strawberry and put it in the container, followed by lowering the arm again. 
After the pick sequence is finished, the robot checks in the same direction for the berry in case it missed picking it and then retrying to pick the berry before moving on. 

Following the successful picking sequence, it starts turning counterclockwise by 10° until it sees the Aruco marker again. 
When the marker is detected, the robot drives for 10cm towards the marker, 
before it turns 90° clockwise to start the strawberries checking prosess again. 
This sequence of picking and checking keeps repeating until the robot is within a certain range of marker 1. 
When The robot has marker 1 in sight and is within it’s minimum range, state 2 is activated by updating the state variable to state 2. 
The robot then turns 70 degrees clockwise to start looking for marker 2. 
State 1 is now completed.

State 2
^^^^^^^^^^^
State 2 is the shortest state in the program. It merely consists of a sequence which makes the robot drive towards Aruco marker 2. 
The robot starts searching for Aruco marker 2 in the same way it searched for marker 1, by turning counter clockwise by 10 degrees then pausing to look, until it finds the marker.
When Aruco marker 2 is in sight, the robot drives towards the marker by a small distance, then checks again if it sees the marker. 
After the robot has driven until to within a certain range of Aruco marker 2. 
The robot turns 90° clockwise and updates the state variable to state 3.

State 3 and 4
^^^^^^^^^^^^^^^^^^^^
The final state is similar to the first state. The only difference being the robot now searching for Aruco marker 3 instead of 1.  
When the robot reaches a certain distance from marker 3, the robot goes in to state 4, 
witch is the final state where the robot prints to the console that it is done with the strawberry picking.

ROS setup
-----------
ROS stands for "Robot Operating System".
It is a program based on the Linux operating system.
ROS receives, computes and send data collected by the linux system, so it is not a real-time system. 
The ROS system is based on communicating with nodes created by the user. 
Nodes are used to either gather data or send data.
A gathering node gather's data from sources, such as a sensor. 
Likewise, a receiving node receives data and is often used to control one component or more, such as a servomotor.
The gatherings node calculates the new sensor data with the system's iteration frequency, if not stated otherwise by the code writer.
A node can either publish, subscribe or do both. If the node publishes, it is simply a manner of making the type of data which it collects,
available for other nodes to subscribe to.
The subscriber node can then subscribe to a publisher node and specific what data it wants to review from the node.
As briefly mentioned, the publish and subscribe frequencies can be altered to suit different needs.
Often nodes sends data when certain instances occurs.

 .. figure:: figures/RosNodeExample.JPG
   :name: NodeExample
   :scale: 70 %
   :align: center
   
   Subscriber node example.

As seen in :numref:`NodeExample`, a simple subscriber node is shown. 
This subscriber node is the same used to receive servo setpoints from another node. 
The node is first initiated as a node, then it handles the incoming data which it is going to receive as an array, by using a function. 
Then it subscribes to that specific message. 
This means that when any other node publishes an array as the specific message, the node recieves the message and utilizes the data.
In the end of the node, it is important to keep the node alive and not making it execute only one time at the start, thats why the "rospy.spin()" function is used in the end.

Jetbot Movement
----------------
The movement of the jetbot is managed from the a "jetbotcontroller" node, which a part of is used as an example in figure :numref:`NodeExample`.
Controlling the two servomotors, which is the wheels of the jetbot assembly, is done by utilizing the "adafruit_PCA9685" library.
The servomotors are continuous servomotors, meaning that adjusting their set points is a matter of controlling their speed.
As such, publishing a message to the "jetbotcontroller" node with the servo setpoints within their max and min range, is going to make the robot move.
The mid numbers of the setpoint range is used to make the jetbot stop.

Arm Movement
-------------
Movement of the picking arm is based on pretty much the same as the jetbot movement.
If a message is published with servo setpoints for the servomotor(s) controlling the arm, then the arm is going to move.
A key difference between the jetbot movement and the arm movement is that the arm movement is controlled by not continuous servomotor(s).
This makes the need of sending setpoints to make the motor(s) stop unnecessary and instead the servo setpoints makes the motor turn to a specific degree.

The arm is designed to be lowered at the start of the program, before the 
main cycle starts. 
Then when picking up a strawberry the arm is 
gradually lifted to a point where the strawberry likely is securely in the arm, then the whole jetbot backs up a bit before the strawberry is thrown into the basket with a swift movement of the arm. 
The arm is then lowered back to the ground.

Aruco Marker Detection
-----------------------
Detection of aruco markers are done in the JetbotCamera node which was provided initially by the lecturers. 
The raw image from the camera is first calibrated, both for color and intrinsic calibration to get rid off the fish-eye effect. 
With the calibrated image, the detectMarkers function from the Aruco library is then used to identify Aruco markers of the 4x4 dictionary. 
The function gives out the id and the position of the corners of the detected markers, which is then passed on to the estimatePoseSingleMarkers 
function along with the marker size of 11 cm which gives out both the position and the pose of the markers. 
 
The only thing that needed to be added to the node was a way for the desired data from the detection to be used in the main node. 
For this purpose a new message type called "vectors" was created, which cointans the float32 values x, y and z and the int32 value "id". 
To get the message type working it had to be added to the "add_message_files" function in CMakeLists.txt. Then the c code had to be recompiled. 
The detection algorithm gave out the ID of the aruco, the postion, and the pose, but It was deemed neccesary only to use the id and position for 
futher work in the main node.

.. figure:: figures/vectordata.png
   :scale: 80 %
   :alt: the image description below will have to do

   shows where the code for publishing the markers id and position is placed

It was assumed that the jetbot will normally only see one aruco at a time, and to make it easy the publishing of the data was placed 
beneath the aleady present printing of the coordinates. Below that, an else statement was added that publishes z = 0 if no aruco was found. 
Should the jetbot happen to see two or more arucos at the same time, the data for one would either overwrite the data from the last one or 
be dismissed due to the qeue size being only one. During testing this did, however, not make any problems for the jetbot.

Strawberry Detection
----------------------
Detection of strawberries was done with the StrawberryDetector class provided by the lecturer.
In short, what the code in the StrawberryDetector class does is this, it starts by slightly blurring the image, then it converts the image so that red color within 
the specified range turns to white while all other colors turn to black. Next, small gaps are filled and specks are removed before it finds 
the biggest contour in the picture. Then the position of this contour relative to the camera is calculated with the PnP algorithm based on 
the assumed radius of a strawberry.

A problem encountered was that the robot detected things that were not strawberries. To eliminate the problem, the limits for how red something has to 
be for the strawberry detection to register a strawberry was increased, but eventually it was reverted back to it's original values.
It turned out to be better to restrict which "strawberries" was to be picked based on the distance towards them.

When the jetbot evaluates whether it sees a strawberry it should pick at the current time, it 
checks whether it sees a strawberry, the distance to the strawberry and the strawberry position in the horizontal plane.
This is done 8 times inside a for loop and if the conditions are true at least 5 times, plus once after the loop, it will run the function 
for picking the strawberry.

Functions
-----------
Several functions were used in the code to make the main code be more straightforwardly and to make the debugging easier. 
The functions were intended to do tasks which would always be done in the same matter in the different states, with a few having inputs. 
Functions used is as listed:

#. CheckStrawberry()
#. PickStrawberry()
#. DriveStraight(Meters)
#. ReverseStraight(Meters)
#. TurnCW(Degrees)
#. TurnCCW(Degrees)
#. TurnToMarker(id)

CheckStrawberry()
^^^^^^^^^^^^^^^^^^^^

CheckStrawberry() is a short function made to make the Jetbot look for a strawberry and make a decision if it is supposed tt pick the strawberry or not.
The function utilizes the StrawberryDetector class as mentioned earlier in the Strawberry Detection section.
If the function "succeeds", a boolean value of "True" is returned, if not, boolean value of "False" is returned.
Even though the function is short, several safety mechanics had to be used to not make the function return "True" without actually seeing a strawberry.
Firstly the camera could read false berries if the camera were in motion during the detection, which may be due to the picture being stretched or altered by the speed in some way.
Therefore, a short break were added in the function to ensure the camera standing still when looking for the strawberry.
Secondly, the function has to see the strawberry 5 times in a row, to ensure that no disturbances can make the function succeed.
Third and final, is a distance control, which makes the robot not able to see red object far away and accept them as a strawberry.
Several times under testing the jetbot mistook parts of a brick wall as a strawberry.
Lastly the function is made in such a manner that it only looks in a certain view, to not make the robot see the "neighbor" strawberries.
The view were tuned greatly to make the function operate properly. 

PickStrawberry()
^^^^^^^^^^^^^^^^^^
The function's name explains itself, its a function which is a picking sequence.
The function first checks if the strawberry is there, the distance and the course.
Further, the jetbot drives towards the strawberry by evaluating the distance and then choosing between making turns or rotating.
Since the arm movement is a circular pattern, the picking distance has to be adjusted dependent on the berry's height.
Even though it should be a trigonometric calculation, a linear calculation worked well since the heights were a minor fraction of the arm movement's radius.
If the strawberry is further than the picking distance, the jetbot turns towards it and if the distance is equal to the picking distance, the jetbot only turns.
All the commands for driving and turning is done by using the servo setpoints, explained earlier, directly.
When the Jetbot is in position, a boolean value "True" is saved as a "in position" variable. 
The "in position" variable activates the arm sequence.
The arm sequence had to be tuned to make the system not behave as a catapult, but is mainly a smooth motion of the arm using small servo steps and pauses everyother.
When the jetbot is done picking, the arm returns to it's initial position, which is low down by the floor.
Finaly the function returns a boolean value of "True", to state that the berry were picked and end the function.

DriveStraight(Meters)
^^^^^^^^^^^^^^^^^^^^^^^^^^
DriveStraight is a simple function to make the jetbot drive straight.
The function started out as a straight drive, which where only a function of servo setpoints and an input "meters".
"Meters" is a bit self explanatory, but simply the speed of the jetbot with certain specific servo setpoints were constant, 
so a calculation were used to acquire the time.
The time were then used as an end time and thereby the wanted distance traveled could be used as an input.
Further, it was not enough that the jetbot just drove straight, it was needed that it got on a course towards the Aruco marker.
For this to happen, the Aruco marker detection were used.
The jetbot then made driven turns, making a smooth course towards the Aruco marker.
At the end of the function, the servor motors were left in a stop position.

ReverseStraight(Meters)
^^^^^^^^^^^^^^^^^^^^^^^^^^
Pretty self explanatory function, and is as easy as it sounds. 
The function simply drives a distance innputed in the function, called "meters".
Similar to DriveStraight, the function utilizes certain servo setpoints for both servomotors and the 
time to make the jetbot travel the desired distance.
At the end of the function, the jetbot is left in a stop position.

TurnCW(Degrees)
^^^^^^^^^^^^^^^^^^^^
The function makes the robot turn clockwise at the same position. 
Degrees is used as input in the function.
In the same manner as the DriveStraight and ReverseStraight function, the rotation speed of certain servo setpoints is used to calculate the degrees rotated.
The jetbot's servo motors is left in a stop position at the end of the function.

TurnCCW(Degrees)
^^^^^^^^^^^^^^^^^^^^
Exactly the same as the TurnCW function, except it rotates counterclockwise instead of clockwise.
The function had to be made with the opposite servo setpoints as TurnCW, since time is used as a variable in the function, making it complicated to use time as a variable.
This however worked out perfectly and required lot less work.

TurnToMarker(id)
^^^^^^^^^^^^^^^^^^^^
TurnToMarker is a simple function, making the jetbot turn until it sees the desired Aruco marker.
The desired Aruco marker is inputted as "id" and the jetbot turns 10 degrees counterclockwise until it has the id in a pretty straight forward course.
Tuning had to be done to not make the sight for the Aruco marker to be narrowed in, achieving a straight enough course.
