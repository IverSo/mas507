Project Management
===================
Project management is often overlooked by development engineers. 
It is a part of product development that is often overlooked when done well by the people responsible. 
In this project different ways of project management was put to use.     

Trello Boards
---------------
The classic wall of post-it notes is a good way of keeping control of what tasks remain, what is being worked on, and what has been finished.
However, a physical wall of post-it notes do require the participants of the project to be present.
This can become problematic when participants decide to, or is required to work from home.
In these uncertain times this has become particularly important to address.

.. _Trello.com: https://trello.com/b/r4hTzySj/mas507-jordb%C3%A6rplukker

Modern problems require modern solutions, so people have given the classic post-it wall a new life, by making it digital. 
`Trello.com`_ is a website where you can create a digital post-it wall. 
The board is placed on a remote server and can be edited by everyone that have been given access.
The trello board used in this project was split into three columns. 

#. Column: All tasks remaining that has not been started.
#. Column: All tasks currently being worked on.
#. Column: All tasks that have been finished.



.. figure:: figures/Trelloboard.jpg
   :scale: 50 %
   :alt: Trelloboard picture

   Trello Board Example `Trello Board Link`_.

DevOps version control
------------------------
When developing software for a robot, a common problem is version control. 
If several people is working on the same code, several different version of the code can develop over time.
This can become problematic when the team decide to join the code to a working build. This process can cost a lot of time and money, 
especially when the developer team is large, due to the code essentially needs to be rewritten.   
    
GitLab
^^^^^^^^
GitLab is a DevOps program that lets you host the repository online. The program is based on Git, where you operate several branches of code. You often set up one branch to be the 
main branch. This is where the code is placed when you know it is working. To handle the problems you get when several people is working on the same code, you create branches of 
the master for them to work on. When a programmer developes on a separate branch, the master is not updated until the branch gets merged. When a branch is merged, means that the 
code on the branch is placed inside the master code. When done correctly, the merging software detects whats new and places it in the spot you wrote it. If several people have 
worked on the exact same code, the git system can get a merging error. This means that Git can not evaluate what the new addition is and the person doing the merging needs to 
specify what part to keep, and what part to delete. 

GitKraken
^^^^^^^^^^
GitKraken is a program that works with Git. It is essentially a user interface to easier have control over the branch system.
Git is a very powerful tool in software development, but it is easy to make a mistake. The mistakes can be very dire, especially for inexperienced users.
Due to all the members of this project being new to Git, git kraken was used to keep control.

There happened several times during the project that something was done incorrectly when handling the Git system.
But due to gitKraken, it was easier to spot the mistake. Without the user interface, trying to fix the mistake could have made things worse.
A branch tree for our project is showed in ::numref:`gitKraken`. 

 .. figure:: figures/gitKraken.jpg
   :name: gitKraken
   :scale: 70 %
   :align: center
   
   Layout of the branch system in GitKraken