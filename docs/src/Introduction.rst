Abstract
=============
In this project a strawberry picking robot was developed and tested in an approximation of a strawberry field.
The robot system included a locomotion system, 
strawberry picking system and two different machine vision applications for detecting strawberries and Aruco markers.
A robot base with to working wheel actuators and camera were provided at project start. 
A arm capable of picking strawberries needed to designed, and an autonomous system needed to be programmed.

A design with one needed actuator were chosen for the arm. Autonomous system were developed to complete the task.  
The robot were largely able to complete the task assigned, but had problems being consistent, 
largely due to the machine vision systems not being good enough.  