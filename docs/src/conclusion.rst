Further work
=============
Although the robot did not totally complete the strawberry picking task, it wasn't far of.  
Due to time constraints, there were several possible improvement that could have been implemented.
Although none of the problems ocurred often, there were enough of them to make the strawberry picking process too inconsistent.
The strawberry picking testbed was also a source of inconstency. The magnets on some strawberries had fallen of and was replaced by tape.
The tape gave a lot of variation on the force required to release the strawberry. 

The most important area of improvement was the vision system. Both the strawberry detection and Aruco marker system had problems. 
The strawberry detection system was set to show only the largest strawberry in range.
This turned out to be a problem when two or more strawberries were too close to each other. 
The robot would sometimes end up driving to a spot between the strawberries.
This happened when the strawberries were found to be almost the same size. This made the vision system flip between showing the two berries.
A possible solution to this problem could have been to rewrite the strawberry detection code to give the right most strawberry.
This could however become a problem if the vision system got a false reading form something in the background.
When the robot positioned itself to pick the strawberry, the strawberry detection could fail. 
The detection system would send an estimate of how close and how high the strawberry was. 
The system would however give the wrong distance or height some times. 
This problem could be a result of noisy strawberry detection signal and since the code only checking the height one time. The sampled height could have been a very large deviation.
A solution to the problem could have been to make the robot check the height several times and take the average value.   
 
The robot had problems detecting the Aruco marker. This was not a problem often, because the robot had a code where it would continue to check again until it found it. 
It would sometimes miss the Aruco on the first try and continue to turn until the Aruco showed up again.
A possible solution to the Aruco marker problem could be to increase the time the robot gets to detect the marker. 
Even though the marker is detected, the detection system needs time to give the right signals.

A problem with the arm design was that the strawberries could launch when it released from the bush. 
It was found that setting the arm speed to max mostly eliminated this problem, but when the force needed to release the strawberry was too big, the strawberry would still launch. 
This was somewhat solved during the testing of the robot, by wall of paper around the container was implemented. This problem was largely caused by the magnets not working and the 
strawberries having to be connectet with tape. A way of eliminating the problem would be to continue to develop a more secure container based on where we saw strawberries escape, 
and/or connecting all berries to the bush with magnets.   

Conclusion
=============
A code for making the robot autonomy were developed, where it was able to detect Aruco markers and drive towards them. 
A system for detecting strawberries and position correctly for picking them were developed, and a working berry picking sequence.
The robot was able to flow between the operation states without problems. The robot wasn't always able to detect the strawberries or the Aruco markers.
The robot would then, depending on the state, try again or continue to the next state without crashing the robot.      

When testing the robot, the detection systems were the source of most of the failure in our control.
There were however a quite large source of failures form the testing setup, making the final optimizing problematic.

All things considered, the robot worked as intended. Two strawberry picking systems were designed, both with only one required servo signal. 
One with more robust parts and potentially more reliable strawberry securement system. The second with a light weight design for easy prototyping and minimal servo load.
The second design was chosen in the end, due to the strawberry securement system was deemed to be satisfactorily secure.