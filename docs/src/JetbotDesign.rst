Jetbot Design
==============
This chapter will explain the jetbot, the mechanical design of the parts, and concepts and prototyping of the robot arm and basket.

Jetbot Hardware
----------------
Most of the hardware involved in this project was supplied by the lecturer. This chapter will explain the different physical parts that make up the robot.

Jetbot
^^^^^^^
The device used is an NVIDIA Jetson Nano. It is an embedded system-on-module, which means it is a working system on a single module. It has a 128-core GPU (Not used), 
a 4-core ARM 64-bit CPU, 4GB of LPDDR4 memory, and plenty of I/O including camera connection, USB, Ethernet, HDMI, and PCIe connections. The system is running Linux. 
All the code will be running on the jetson, and control all the other compoenents.  

Case
^^^^^
The case is provided by NVIDIA, the manufacturer of the Jetson Nano. It has been modified to allow for attachment of the servos and arms. The case has a basket attached 
to its side, to store picked strawerries. The basket is self designed and 3D printed.

Camera
^^^^^^^
The camera used ia an LI-IMX219-MIPI-FF-NANO. It is a 3280x2464 resolution camera operating at 21 fps and at 1.2-2.8V. This is used for image recognition, to localize 
strawberries and Aruco markers. The camera is mounted on the jetbot on top of the front of the robot. It was also angled slightly downwards, but this was changed to a vertical mount. 
It is mounted with four screws to a 3D printed bracket.

Servos
^^^^^^^
The servos used are JX PDI-1171MG. They are 17g servos operating at 4.8-6.0V. They have a maximum rotational speed of 0.11 sec/60°, and a maximum torque of 0,34Nm 
at 6.0V. They are intended for radio controlled models and small robots. In this project they are used to operate the arm and move the strawberries from the bush to the basket.

Arm
^^^^
The arm is self designed and the design process is explained in the next chapter. It has a small basket on the end of a beam hinged at floor level, and angled sideways to put the 
strawberry in the basket on the side of the robot.

Arm Design
-----------
This chapter will explain the concept phase of the arm design, which solutions were considered, and which solutions was decided to develop further.

Concepts
^^^^^^^^^
Different concepts were discussed for the arm. There were three servos provided, so there could be a maximum of three powered joints.

Concept 1
""""""""""
The first concept is based on using the minimum number of servos; one. This is done to minimize coding time, in exchange for a slighly more complex, and perhaps less accurate 
arm. Its mass is minimized to allow for quick 3D printing and therefore quick prototyping. Also a reason for its low mass is the servos. They are small and weak and the weight 
of the arm sould be minimized to maximize their chance of survival. Concept 1 was printed first and as it proved to work well it was decided to stick to this concept. An improved 
arm was printed later and after some testing and improvements it was fairly accurate. 

Pros:

- Fast print

- Small servo load

- Simple code required

- Few movable parts

Cons:

- Inaccurate strawberry release

- Not as robust as other concepts

.. figure:: figures/ArmGIF.gif
   :scale: 70 %
   :alt: Robot Arm Overview 

   The concept 1 Robot Arm.

Concept 2
""""""""""
The second concept is designed for mechanical robustness. It also has only one input (Devided into two servos, so code is still relatively simple). The release of the strawberry is 
based on the basket on the arm hittig a wall and therefore rotating to release the strawberry.

Pros:

- Robust

- Simple code requred

- Few parts

- Simple mechanical strawberry release

Cons:

- Big and heavy

- Might cover up camera/might have to move camera

- Uses two servos

.. figure:: figures/Concept2GIF.gif
   :scale: 100 %
   :alt: Concept 2 Movement

   The movement of the concept 2 arm.

Concept 3
""""""""""
Concept 3 is similar to concept 2, but includes a servo to grab and release the strawberry. This concept will have a more complex coding requirement, but in return might 
have better results. It will have many large parts and therefore longer printing time, and will in turn be more robust. 

Pros:

- Robust

- Few parts

- accurate strawberry release


Cons:

- More complex code

- Big and heavy

- Might cover up camera

- Uses three servos


Testing
--------
During prototype testing there were many problems we had to consider and overcome. This segment will explain some of the bigger problems faced during testing, and the solutions 
implemented to overcome said challenges.

Strawberries falling off
^^^^^^^^^^^^^^^^^^^^^^^^^
While rotating the arm towards the basket the berry would often fall off the arm partly due to oscillations from the servo, but mostly because the thin arm 
would become spring loaded and lauch the berry unconrollaby when the magnet released. This gave us the idea of a fast moving arm, which turned out very accurate and easy to 
tune in the end. A new stiffer arm was designed and printed, and the code was changed to move the arm faster.

Wheels slipping
^^^^^^^^^^^^^^^^
One of the problems encountered was the wheels slipping on the floor, and therefore the robot did not align properly with the berries. This was caused 
by the rear wheel not turning properly and a general rearwards center of gravity. This was solved by adding weight to the robot, over the wheels and 
counteracting the weight of the basket. We also wiped the wheels clean of dust and wiped the floor. It made the turning of the robot considerably better. 

Strawberries flying too far
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
A catch "fence" had to be added behind the basket, to catch strawberries that were going too far. A piece of paper was added to the rear of the basket. This turned 
out to be a great solution, and an added benefit was the damping cheracteristics of the paper, to prevent strawberries from flying further after hitting it. 

Camera angled too far downwards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Another problem that surfaced during testing was the angle of the camera. The field of view was facing down too much and preventing the camera recognition from working properly. 
A solution was to 3D print a new vertical camera mount. This new part was installed and the camera recognition worked better as a result. 

Servo wire deterction
^^^^^^^^^^^^^^^^^^^^^^
Due to the servo being positioned in front of the camera, the strawberry recognition would sometimes recognize the red wire as a strawberry. The solution was simple, the cable was 
wrapped in tape to cover the red wire.

.. figure:: figures/PickingGIF.gif
   :scale: 100 %
   :alt: Strawberry path 

   Demo of how the robot picks strawberries.
